## Servidor de Arquivos Basico com SAMBA


## Introducão

O Samba é um conjunto de software livre / código aberto que, desde 1992 , fornece serviços de arquivo e impressão para todos os tipos de clientes SMB / CIFS, incluindo as inúmeras versões dos sistemas operacionais Microsoft Windows. O Samba está disponível gratuitamente sob a GNU General Public License .


Informações básicas 

O totorial foi criado com intuito de disponibilizar um servidor de arquivos smb funcinoal, infelizmente nao vou abordar com detalhes cada cfg e sua funcao, mas o basico sera passado. Segue as informacões do ambiente usado.

* Sistema Operacional GNU/Linux Debian10 (Buster) amd64
* Instalação limpa (netinstall), apenas ferramentas basicas do sistema e ssh.
* Por questões de boa pratica e segurança não habilite o root na instalacão, use um usuário com o sudo ativado.

O fucionalidades do servidor 

* Pasta privada com usuario e senha
* Pasta publica
* Lixeira (com versionamente de arquivos e auto lipeza de 90 dias)
* LOGs completos (full-audit)



# Preparando o Sistema

Deixe seu OS atualizado:
```
$ sudo apt update && sudo apt full-upgrade -y
```
Instale os seguintes pacotes:
```
$ sudo apt install samba samba-vfs-modules
```

Defina o nome do seu servidor, editando os arquivos `hostname` e `hosts` 
```
$ sudo nano /etc/hostname
```
```
127.0.0.1       localhost
127.0.1.1       fileserver_samba

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters

```
```
$ sudo nano /etc/hosts
```
```
fileserver_samba
```
## Criando diretorios

Diretorio de exemplo, pasta publica e lixeira:
```
$ sudo mkdir /srv/dirtest
$ sudo mkdir /srv/lixeira
$ sudo mkdir /srv/publico
```

## Configurando o SAMBA

Todas as configurações do samba são feitas em um único arquivo o `smb.conf`, você pode deletar todo o conteúdo do mesmo e substituir pelos exemplos a seguir.

OBS: As seguinte configuração Global é o padrão funcional do samba na distro debian10, está apenas "limpo" e organizado.
```
$ sudo nano /etc/samba/smb.conf
```

### Configurações Globais
```
#=========== Global Settings

[global]

   workgroup = WORKGROUP
   log file = /var/log/samba/log.%m
   max log size = 1000
   logging = file
   panic action = /usr/share/samba/panic-action %d
   server role = standalone server
   obey pam restrictions = yes
   unix password sync = yes
   passwd program = /usr/bin/passwd %u
   passwd chat = *Enter\snew\s*\spassword:* %n\n *Retype\snew\s*\spassword:* %n\n *password\supdated\ssuccessfully* .
   pam password change = yes
   map to guest = bad user
   usershare allow guests = yes
```
### Veto de Arquivos 

Impedirá a copia de arquivos para o compartilhamento tendo como critério a extensão.


Nesse nosso exemplo o veto de arquivos sera aplicado pra todas as pastas compartilhadas.
Para veto por pasta essa cfg tem que sair da sessão Global e ser aplicada nas sessões individuais das pastas compartilhadas. 


```
#=========== Veto de Arquivos

        veto files = /*.exe/*.pif/*.bat/*.vbs/*.cmd/*.scr/*.ws/*.reg/*.hta/*.msi/*.com/*.pif/*.js/*.wsf/*.cpl/*.jar/Thumbs.db/
        hide files = yes
        
        
```
`OBS: pode ser adicionados mais extensões a regra, basta seguir o padrão`

### LOGs e lixeira

A opcao `recycle:repository`, deve ser apontada paro o diretorio destinado a lixeira.

A opção `recycle:exclude`, excluirá arquivos automaticamente da lixeira tendo como critério a extensão, altere conforme sua necessidade.

```
#=========== LOGs mais completos e lixeira

        vfs objects = full_audit, recycle, crossrename   
        full_audit:success = open, opendir, write, unlink, rename, mkdir, rmdir, chmod, chown
        full_audit:prefix = %u|%I|%S
        full_audit:failure = none
        full_audit:facility = local5
        full_audit:priority = notice
        
#=========== Lixeira

        crossrename: sizelimit = 2000
        recycle:keeptree = yes
        recycle:versions = yes
        recycle:repository = /srv/lixeira
        recycle:exclude = *.tmp, *.log, *.obj, ~*.*, *.bak, *.iso
        recycle:exclude_dir = tmp, cache
        recycle:directory_mode = 0777
        recycle:subdir_mode = 0777
        ecycle:touch = true
```

`**Existe configurações extras`

### Compartilhamento de pasta
* `[]` nome da pasta que vai aparecer no compartilhamento
* `path` caminho do diretório que vai ser compartilhado
* `valid users` usuários ou grupos validos 
* `browseable` se pasta pode ser visível no compartilhamento
* `writeable` se os arquivos da pasta podem sem modificados
* `read only` se a pasta é apenas leitura
* `guest ok` se uma conta convidado/anomino podem acessar o compatilhamento
* `inherit permissions` toda vez que uma nova pasta ou arquivo for criado a permissão da pasta pai sera herdada

```
#=========== Pastas Compartilhadas


[lixeira]
        path = /srv/lixeira
        valid users = @testegrp
        browseable = yes
        writeable = no
        read only = yes
        guest ok = no

[publica]
        path = /srv/publico
        browseable = yes
        writeable = yes
        read only = no
        guest ok = yes
        
[pasta_compart]
        path = /srv/dirtest
        valid users = @testegrp
        browseable = yes
        writeable = yes
        read only = no
        guest ok = yes
        inherit permissions = yes
     
        
```

## Usuario, Grupos e Permissões



## Configuraçoes Extras e Scripts de automação

sudo apt install unattended-upgrades
sudo apt install logrotate / sudo nano /etc/logrotate.d/samba
sudo ls -l /var/log/
sudo nano /etc/samba/script_lixeira
sudo nano /etc/samba/smb.conf 
sudo nano /etc/samba/script_lixeira
sudo chmod +x /etc/samba/script_lixeira 
sudo nano /etc/crontab 
sudo nano /etc/samba/script_ppublica
sudo chmod +x /etc/samba/script_ppublica 
cat /etc/samba/smb.conf
sudo nano /etc/samba/script_ppublica

sudo cat /etc/rsyslog.d/sambafull_audit.conf 
local5.notice /var/log/samba-full_audit.log

sudo cat /etc/logrotate.d/samba.audit 
/var/log/samba-full_audit.log {
        weekly
        rotate 20
        size 20M
        compress
        delaycompress
}








